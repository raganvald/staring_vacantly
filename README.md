# Ludum Dare 44: Minimum Viable Piggy!

Minimum Viable Piggy is our submission for the Ludum Dare 44 Game Jam. Follow
Hamilton the Pig on his journey through the kitchen and battle the chefs trying
cook our portly porker before he gets all the truffles they have! Our pig is
constantly on the move and needs the truffles to stay alive, the skinnier the
swine, the faster he's determined to get his snacks. But watch out, don't be
too greedy or our bloated boar will slow down, making it easier for tonight's
dinner to be a luau!

## Controls
* Enter: Start the game
* Arrow Keys or WASD: Move the pig around
* Space or F: attack


## References

- [Godot 3 2D Platform Game] (playlist)
- [GDScript reference]
- [Godot 2D tutorial]
- [Godot 3 Autotiling]


[Godot 3 2D Platform Game]: https://www.youtube.com/watch?v=wETY5_9kFtA&list=PL9FzW-m48fn2jlBu_0DRh7PvAt-GULEmd
[GDScript reference]: https://docs.godotengine.org/en/3.1/getting_started/scripting/gdscript/index.html
[Godot 2D tutorial]: https://docs.godotengine.org/en/latest/tutorials/2d/index.html
[BeepBox]: https://www.beepbox.co/
[Bosca Ceoil]: https://boscaceoil.net/
[Godot 3 Autotiling]: https://michagamedev.wordpress.com/2018/02/24/181/
