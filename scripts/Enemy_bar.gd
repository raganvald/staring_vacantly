extends TextureProgress

var animated_health

func _ready():
	max_value = get_parent().MAX_HEALTH
	value = max_value
	animated_health = max_value

func updateHealth(newVal):
	$Tween.interpolate_property(self, "animated_health", animated_health, newVal, 0.1, Tween.TRANS_EXPO, Tween.EASE_IN_OUT)
	if not $Tween.is_active():
		$Tween.start()

func _on_Tween_tween_step(object, key, elapsed, val):
	value = round(animated_health)
