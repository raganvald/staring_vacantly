extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var rng = RandomNumberGenerator.new()
var enemy_scene = load("res://scenes/Enemy.tscn")
const MAX_ENEMIES = 10

export (float) var spawn_probability = 0.01
export (int) var enemy_damage = 10

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func spawn_enemies():
#func _on_Timer_timeout():
	var rand = rng.randf()
	var colshape = get_child(0)
	var size = colshape.shape.extents
	if rand < spawn_probability:
		var enemy: Node2D = enemy_scene.instance()
		var enemy_body: KinematicBody2D = enemy.get_child(0)
		enemy_body.damage = enemy_damage
		var pos = Vector2()
		pos.x = (rng.randi() % int(size.x)) - (size.x / 2) + position.x + colshape.position.x
		pos.y = (rng.randi() % int(size.y)) - (size.y / 2) + position.y + colshape.position.y
		enemy_body.kill_y = int(pos.y) + 1000
		enemy_body.position = pos
		get_parent().add_child(enemy)
