extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_VomitBody_body_entered(body):
	print("Body entered vomit body")
	if !body.get_groups().has("floors"):
		body.take_hit()
	queue_free()


func _on_VomitArea_body_exited(body):
	if body == $VomitBody:
		print("Vomit body left vomit area")
		queue_free()


func get_body():
	return $VomitBody

