extends Node

var rng = RandomNumberGenerator.new()

var sound_effects = {
    "pigjump":[
        "pigjump1",
        "pigjump2",
        "pigjump3",
        "pigjump4",
        "pigjump5"
    ],
    "pigfart": [
        "pigfart1",
        "pigfart2",
        "pigfart3"
    ],
    "pighurt":[
        "pighurt1",
        "pighurt2",
        "pighurt3",
        "pighurt4"
    ],
    "pigtruffleget":[
        "pigtruffleget1",
        "pigtruffleget2",
        "pigtruffleget3",
        "pigtruffleget4"
    ],
    "uihover":["uihover"],
    "uiclick":["uiclick"],
	"scenechange":["scenechange"],
	"youdied":["youdied"]
}

# Called when the node enters the scene tree for the first time.
func _ready():
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("Low Music"), true) 
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("Med Music"), true) 
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("High Music"), true) 

func play_low_music():
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("Low Music"), false) 
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("Med Music"), true) 
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("High Music"), true) 


func play_med_music():
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("Low Music"), true) 
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("Med Music"), false) 
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("High Music"), true) 


func play_high_music():
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("Low Music"), true) 
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("Med Music"), true) 
    AudioServer.set_bus_mute (AudioServer.get_bus_index ("High Music"), false) 


func play_vomit(to_play):
	if to_play:
		AudioServer.set_bus_volume_db (AudioServer.get_bus_index ("Vomit"), 0.0)
	else:
		AudioServer.set_bus_volume_db (AudioServer.get_bus_index ("Vomit"), -80.0)


func play_sound_effect(sound_name):
    # reference sound effect by the key in the dictionary
    # and choose a node to randomly play from the list
    # get node name
    assert(sound_effects.has(sound_name))

    var snd_index = rng.randi_range(0, len(sound_effects[sound_name]) - 1 )
    var snd_node_name = sound_effects[sound_name][snd_index]

    var node_to_play = get_node("/root/AudioManager/SFX/"+snd_node_name)
    node_to_play.play()
