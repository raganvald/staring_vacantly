extends RigidBody2D

# Member variables

func _ready():
	pass

func _process(delta):
	if position.y > 1000 or position.x > 2000:
		print("Free bullet")
		queue_free()

func _on_BulletArea_body_entered(body):
	if !body.get_groups().has("floors"):
		body.take_hit()
	queue_free()
