extends KinematicBody2D

#signals
signal updateHealth
signal dead

#constants 
const UP = Vector2(0, -1)
const GRAVITY = 20
const ACCELERATION = 30
const MIN_ACCELERATION = 1
const MAX_SPEED = 400
const JUMP_HEIGHT = -550
const MAX_JUMPS = 5
const MIN_THICCNESS = 0
const MAX_THICCNESS = 200
const ENEMY_DAMAGE = 5
const SCALE_THICCNESS = 10
const FLOOR_FRICTION = 1
const AIR_FRICTION = 0.01
const VOMIT_SPEED = 100.0
const VOMIT_DAMAGE = -0.3
const JUMP_DAMAGE = -5

#stored variables
export(int) var thiccness = 160
var motion = Vector2()
var jumpCount = 0
var facingLeft = false
var inPain = false
var painCounter = 0 #this is bad and I feel bad. I'm sorry.

#assets to spawn
var bullet = preload("res://scenes/bullet.tscn")
var fart = preload("res://scenes/Fart_cloud.tscn")
var vomit = preload("res://scenes/Vomit.tscn")

#---------------------------------------------------
#-----------------signal handlers-------------------
#---------------------------------------------------
func eatTruffle(food):
	AudioManager.play_sound_effect("pigtruffleget")
	updateThiccness(food)

func _on_Timer_timeout():
	updateThiccness(-1)

func _on_player_dead():
	AudioManager.play_low_music()
	var root = get_tree().get_root()
	get_node("/root/currentScene").current_scene = root.get_child(root.get_child_count() -1).filename
	get_tree().change_scene("res://scenes/dead.tscn")



func take_hit():
	print("TOOK A HIT!!!")
	AudioManager.play_sound_effect("pighurt")
	updateThiccness(-ENEMY_DAMAGE)
	inPain = true
	motion.y = -300
	if facingLeft:
		motion.x = 500
	else:
		motion.x = -500


#---------------------------------------------------
#-----------------helper functions------------------
#---------------------------------------------------
func jump():
	if jumpCount < MAX_JUMPS:
		motion.y = JUMP_HEIGHT
		jumpCount += 1

		if jumpCount > 1:
			updateThiccness(JUMP_DAMAGE*(jumpCount)^2)
			var toot = fart.instance()
			toot.position = $bullet_shoot.position
			if facingLeft:
				toot.rotation_degrees = 50
			add_child(toot)
			AudioManager.play_sound_effect("pigfart")
		else:
			AudioManager.play_sound_effect("pigjump")

#---------------------------------------------------
func updateThiccness(delta):
	thiccness = clamp(thiccness + delta, MIN_THICCNESS, MAX_THICCNESS)
	
	var half_thiccness = (MIN_THICCNESS + MAX_THICCNESS) / 2.0
	if thiccness < half_thiccness:
		AudioManager.play_high_music()
	else:
		AudioManager.play_med_music()
	
	print(thiccness)
	if thiccness == MIN_THICCNESS:
		emit_signal("dead")
		AudioManager.play_sound_effect("youdied")
	else:
		emit_signal("updateHealth", thiccness)

#---------------------------------------------------
var shoot_count = 0
func handleShooting():
	if Input.is_action_pressed("ui_shoot"):
		updateThiccness(VOMIT_DAMAGE)
		shoot_count += 1
		if (shoot_count & 0x3) == 0:
			var leftRight
			#var pigpos = bullet.instance()
			var vomit_inst = vomit.instance()
			var vomit_body = vomit_inst.get_body()
			#pigpos.get_node("Sprite").flip_h = facingLeft
			if facingLeft:
				leftRight = -1.0
			else:
				leftRight = 1.0
			#vomit_inst.rotation_degrees = 180*leftRight
			#vomit_inst.get_node("VomitArea").rotation_degrees = 180*leftRight
			if facingLeft:
				vomit_inst.set_transform(Transform2D.FLIP_X)
			#var pos = position + $bullet_shoot.position * Vector2(leftRight, 1.0)
			#vomit_body.position = pos

			vomit_body.linear_velocity = Vector2(VOMIT_SPEED * leftRight, 0)

			#add_collision_exception_with(vomit_inst) # Make bullet and this not collide
			add_child(vomit_inst)

#---------------------------------------------------
func handleAnimations():
	if inPain:
		$Sprite.play("Pain")
		painCounter += 1
		if painCounter == 10:
			inPain = false
	else:
		painCounter = 0
		#left/right orientation
		if Input.is_action_pressed("ui_right"):
			facingLeft = false
		elif Input.is_action_pressed("ui_left"):
			facingLeft = true
		$Sprite.flip_h = facingLeft
	
		#animation decisions
	
		if is_on_floor():
			if Input.is_action_pressed("ui_right"):
				$Sprite.play("Run")
			elif Input.is_action_pressed("ui_left"):
				$Sprite.play("Run")
			else:
				$Sprite.play("Idle")
		else:
			if motion.y > 0:
				$Sprite.play("Jump down")
			else:
				$Sprite.play("Jump up")


#---------------------------------------------------
const c1: float = 6.5
func acceleration(t):
	return max(ACCELERATION + (-log(t/2)*c1 + log(log(t/2))),
			MIN_ACCELERATION)

func handleMovement():
	#handle up/down velocity
	motion.y += GRAVITY
	if is_on_floor():
		jumpCount = 0
	if Input.is_action_just_pressed("ui_up"):
		jump()

	#handle left/right velocity
	var friction = false
	if Input.is_action_pressed("ui_right"):
		motion.x = min(motion.x+acceleration(thiccness), MAX_SPEED)
		facingLeft = false
	elif Input.is_action_pressed("ui_left"):
		motion.x = max(motion.x-acceleration(thiccness), -MAX_SPEED)
		facingLeft = true
	else:
		friction = true

	#linear interpolation for friction
	if friction:
		if is_on_floor():
			motion.x = lerp(motion.x, 0, FLOOR_FRICTION)
		else:
			motion.x = lerp(motion.x, 0, AIR_FRICTION)

	#apply the motion
	motion = move_and_slide(motion, UP)

#---------------------------------------------------
#-----------------physics callback------------------
#---------------------------------------------------
func _physics_process(delta):
	if position.y > 10000:
		emit_signal("dead")

	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
		
	if Input.is_action_just_pressed("ui_reload"):
		get_tree().reload_current_scene()

	handleMovement()
	handleAnimations()
	handleShooting()
