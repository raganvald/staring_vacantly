extends Area2D

export(int) var nutrition = 10

var eatten = false
onready var player = get_owner().get_node("player")


func _on_truffle_body_entered(body):
	if not eatten and body == player:
		player.eatTruffle(nutrition)
		queue_free()
